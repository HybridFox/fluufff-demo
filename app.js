const { createServer } = require("http");

const PORT = 3000;

const server = createServer();

server.on("request", (request, response) => {
  response.end(`ENV_SPECIES ${process.env.SPECIES}`);
});

server.listen(PORT, () => {
  console.log(`starting server at port ${PORT}`);
});