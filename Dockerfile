FROM node:erbium-alpine

EXPOSE 3000
COPY . .

CMD ["node", "app"]